package com.example.demo.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.web.domain.Question;

public interface QuestionRepository extends CrudRepository<Question, Long> {

}
