package com.example.demo.web;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.repository.QuestionRepository;
import com.example.demo.web.domain.Question;

@RestController
@RequestMapping("/api/questions")
public class QuestionController {

	@Autowired
	private QuestionRepository quesRepo;

	@GetMapping
	public Iterable<Question> getQuestions() {
		Iterable<Question> questions = quesRepo.findAll();
		return questions;
	}

	@PostMapping
	public ResponseEntity<Question> addQuestion(@Valid @RequestBody Question question) {
		Question q = quesRepo.save(question);
		return new ResponseEntity<Question>(q, HttpStatus.CREATED);
	}

}
