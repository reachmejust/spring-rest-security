package com.example.demo.web.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Question {

	@Id
	@GeneratedValue
	@Column(name="QUESTION_ID")
	private Long id;

	@NotNull
	private String prompt;
	private String description;
	@NotNull
	private String type;

	public Question() {
		super();
	}

	public Question(String prompt, String type) {
		super();
		this.prompt = prompt;
		this.type = type;
	}

	public Question(Long id, String prompt, String description, String type) {
		super();
		this.id = id;
		this.prompt = prompt;
		this.description = description;
		this.type = type;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPrompt() {
		return prompt;
	}

	public void setPrompt(String prompt) {
		this.prompt = prompt;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
